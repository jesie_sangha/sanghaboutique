﻿using SanghaBoutique.Entities;
using SanghaBoutique.Services;
using SanghaBoutique.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanghaBoutique.Web.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductTable(string search, int? pageno)//make pageno nullable
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            pageno = pageno.HasValue ? pageno.Value > 0 ? pageno.Value : 1 : 1;

            var totalRecords = ProductServices.Instance.GetProductsCount(search);
            model.Products = ProductServices.Instance.GetProducts(search, pageno.Value);

            if (model.Products != null)
            {
                model.Pager = new Pager(totalRecords, pageno, 5);
                return PartialView("ProductTable", model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            NewProductViewModel model = new NewProductViewModel();
            model.AvailableCategories = CategoriesServices.Instance.GetAllCategories();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Create(NewProductViewModel model)
        {
            var newProduct = new Product();
            newProduct.Name = model.Name;
            newProduct.Description = model.Description;
            newProduct.price = model.Price;
            newProduct.ImageURL = model.ImageURL;
            newProduct.Category = CategoriesServices.Instance.GetCategory(model.CategoryID);

            ProductServices.Instance.SaveProducts(newProduct);
            return RedirectToAction("ProductTable");
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditProductViewModel model = new EditProductViewModel();
            var product = ProductServices.Instance.GetProduct(ID);
            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.ImageURL = product.ImageURL;
            model.Price = product.price;

            if (product.Category != null)
            {
                model.CategoryID = product.Category.ID;
            }
            else
            {
                model.CategoryID = 0;
            }

            model.AvailableCategories = CategoriesServices.Instance.GetAllCategories();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Edit(EditProductViewModel model)
        {
            var existingProduct = ProductServices.Instance.GetProduct(model.ID);
            existingProduct.Name = model.Name;
            existingProduct.Description = model.Description;
            existingProduct.price = model.Price;
           
            //existingProduct.CategoryID = 0;
            //existingProduct.CategoryID = model.CategoryID;

            if (!string.IsNullOrEmpty(model.ImageURL))
            {
                existingProduct.ImageURL = model.ImageURL;
            }
            ProductServices.Instance.UpdateProduct(existingProduct);
            return RedirectToAction("ProductTable");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            ProductServices.Instance.DeleteProduct(id);
            return RedirectToAction("ProductTable");
        }
        
        [HttpGet]
        public ActionResult Details(int ID)
        {
            ProductViewModel model = new ProductViewModel();
            model.Product = ProductServices.Instance.GetProduct(ID);
            return PartialView(model);
        }
    }
}