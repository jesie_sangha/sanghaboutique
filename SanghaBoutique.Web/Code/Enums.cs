﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SanghaBoutique.Web.Code
{
    public class Enums
    {
        public enum SortByEnums
        {
            Default = 1, 
            Popularity = 2,
            PriceLowToHigh = 3,
            PriceHighToLow = 4,
        }
    }
}