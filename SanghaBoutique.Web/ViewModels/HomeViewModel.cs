﻿using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SanghaBoutique.Web.ViewModels
{
    //This class helps to send data of different entities to same index.cshtml (main page)
    public class HomeViewModel
    {
        public List<Category> FeaturedCategories { get; set; }
        public List<Product> Products { get; set; }
    }
}