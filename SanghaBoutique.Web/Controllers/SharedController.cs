﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanghaBoutique.Web.Controllers
{
    public class SharedController : Controller
    {
        public JsonResult UploadImage()
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet; //return during GET request as well

            try
            {
                var file = Request.Files[0];

                //Guid.NewGuid() will create random string - change the name of file for security reason
                // var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);

                var fileName = file.FileName;
                var path = Path.Combine(Server.MapPath("~/content/images/"), fileName);

                //var fileName1 = Guid.NewGuid() + Path.GetExtension(file.FileName);
                //var path1 = Path.Combine(Server.MapPath("~/content/images/"), fileName);

                

                file.SaveAs(path);


                result.Data = new
                {
                    Success = true,
                    ImageURL = string.Format("/content/images/{0}", fileName)
                };
            }
            catch (Exception ex)
            {
                result.Data = new { Success = false, Message = ex.Message };
            }
            return result;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}