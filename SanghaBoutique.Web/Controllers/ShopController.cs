﻿using SanghaBoutique.Services;
using SanghaBoutique.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using static SanghaBoutique.Web.Code.Enums;

namespace SanghaBoutique.Web.Controllers
{
    public class ShopController : Controller
    {
        public ActionResult Index(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int? pageNo)
        {
            ShopViewModel model = new ShopViewModel();

            model.FeaturedCategories = CategoriesServices.Instance.GetFeaturedCategories();
            model.MaximumPrice = ProductServices.Instance.GetMaximumPrice();
            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;


            model.Products = ProductServices.Instance.SearchProducts(searchTerm, minimumPrice, maximumPrice, categoryID, sortBy, pageNo.Value, 5);
            model.SortBy = sortBy;
            model.CategoryID = categoryID;
            int totalCount = ProductServices.Instance.SearchProductsCount(searchTerm, minimumPrice, maximumPrice, categoryID, sortBy);
            model.Pager = new Pager(totalCount, pageNo);
            return View(model);
        }

        public ActionResult FilterProducts(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int? pageNo)
        {
            FilterProductViewModel model = new FilterProductViewModel();

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;
            model.SortBy = sortBy;
            model.SearchTerm = searchTerm;
            model.CategoryID = categoryID;

            int totalCount = ProductServices.Instance.SearchProductsCount(searchTerm, minimumPrice, maximumPrice, categoryID, sortBy);

            model.Products = ProductServices.Instance.SearchProducts(searchTerm, minimumPrice, maximumPrice, categoryID, sortBy, pageNo.Value, 5);

            model.Pager = new Pager(totalCount, pageNo);

            return PartialView(model);
        }

        [Authorize]
        public ActionResult Checkout()
        {
            CheckoutViewModels cprod = new CheckoutViewModels();
            var CartProductsCookie = Request.Cookies["CartProducts"];
            if (CartProductsCookie != null)
            {
                cprod.CartProductIds = CartProductsCookie.Value.Split('-').Select(x => int.Parse(x)).ToList();
                cprod.CartProducts = ProductServices.Instance.GetProducts(cprod.CartProductIds);
            }
            return View(cprod);
        }

        public ActionResult PlaceOrder()
        {
            HttpCookie currentUserCookie = HttpContext.Request.Cookies["CartProducts"];
            HttpContext.Response.Cookies.Remove("CartProducts");
            if (currentUserCookie != null)
            {
                currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                currentUserCookie.Value = null;
                HttpContext.Response.SetCookie(currentUserCookie);
            }
            return View();
        }
    }
}