﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SanghaBoutique.Web.Startup))]
namespace SanghaBoutique.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
