﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SanghaBoutique.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "ControlPanel",
               url: "control-panel",
               defaults: new { controller = "Categories", action = "CategoryTable" }
           );

            routes.MapRoute(
            name: "deleteProduct",
            url: "hello/jas", //CUSTOM Routing - u can type hello/jas instead of product/delete
            defaults: new { controller = "Product", action = "Delete" }
        );

            routes.MapRoute(
             name: "editProduct",
             url: "Product/edit",
             defaults: new { controller = "Product", action = "Edit" }
         );

            routes.MapRoute(
             name: "createProduct",
             url: "Product/Create",
             defaults: new { controller = "Product", action = "Create" }
         );
            routes.MapRoute(
              name: "SearchProducts",
              url: "Product/ProductTable",
              defaults: new { controller = "Product", action = "ProductTable" }
          );

            routes.MapRoute(
            name: "UploadImageCreateCategory",
            url: "shared/UploadImage",
            defaults: new { controller = "shared", action = "UploadImage" }
        );

            routes.MapRoute(
             name: "deleteCategories",
             url: "Category/Delete",
             defaults: new { controller = "Category", action = "Delete" }
         );

            routes.MapRoute(
             name: "editCategories",
             url: "Category/Edit",
             defaults: new { controller = "Category", action = "Edit" }
         );

            routes.MapRoute(
             name: "createCategories",
             url: "Category/Create",
             defaults: new { controller = "Category", action = "Create" }
         );

            routes.MapRoute(
               name: "SearchCategories",
               url: "Category/CategoryTable",
               defaults: new { controller = "Category", action = "CategoryTable" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
