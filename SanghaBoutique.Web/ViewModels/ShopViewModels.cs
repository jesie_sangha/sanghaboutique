﻿using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SanghaBoutique.Web.ViewModels
{
    public class CheckoutViewModels
    {
        public List<Product> CartProducts { get; set; }
        public List<int> CartProductIds { get; set; }
        public int CartProductCount { get; set; }
    }
    public class ShopViewModel
    {
        public List<Category> FeaturedCategories { get; set; }
        public List<Product> Products { get; set; }
        public int MaximumPrice { get; set; }
        public int? SortBy { get; set; }
        public int? CategoryID { get; set; }

        public Pager Pager { get; set; }
        public string SearchTerm { get; set; }
    }
    public class FilterProductViewModel
    {
        public List<Product> Products { get; set; }
        public Pager Pager { get; set; }
        public int? CategoryID { get; set; }
        public int? SortBy { get; set; }
        public string SearchTerm { get; set; }
   }
}