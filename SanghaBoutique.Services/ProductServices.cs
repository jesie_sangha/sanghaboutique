﻿using SanghaBoutique.Database;
using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SanghaBoutique.Services
{
    public class ProductServices
    {
        #region Singleton
        public static ProductServices Instance
        {
            get
            {
                if (instance == null)
                    instance = new ProductServices();
                return instance;
            }
        }
        private static ProductServices instance { get; set; } //create private object


        #endregion
        public int GetMaximumPrice()
        {
            using (var context = new SBContext())
            {
                return (int)context.Products.Max(x => x.price);
            }
        }

        public List<Product> SearchProducts(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy, int pageNo,int pageSize)
        {
            using (var context = new SBContext())
            {
                var products = context.Products.ToList();
                if (categoryID.HasValue)
                {
                    products = products.Where(x => x.Category.ID == categoryID.Value).ToList();
                }
                if (!string.IsNullOrEmpty(searchTerm))
                {
                    products = context.Products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
                }
                if (minimumPrice.HasValue)
                {
                    products = products.Where(x => x.price >= minimumPrice.Value).ToList();
                }
                if (maximumPrice.HasValue)
                {
                    products = products.Where(x => x.price <= maximumPrice.Value).ToList();
                }
                if (sortBy.HasValue)
                {
                    switch (sortBy.Value)
                    {
                        case 2:
                            products = products.OrderByDescending(x => x.ID).ToList();
                            break;
                        case 3:
                            products = products.OrderBy(x => x.price).ToList();
                            break;
                        default:
                            products = products.OrderByDescending(x => x.price).ToList();
                            break;
                    }
                }
                return products.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }
        }


        public int SearchProductsCount(string searchTerm, int? minimumPrice, int? maximumPrice, int? categoryID, int? sortBy)
        {
            using (var context = new SBContext())
            {
                var products = context.Products.ToList();
                if (categoryID.HasValue)
                {
                    products = products.Where(x => x.Category.ID == categoryID.Value).ToList();
                }
                if (!string.IsNullOrEmpty(searchTerm))
                {
                    products = context.Products.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
                }
                if (minimumPrice.HasValue)
                {
                    products = products.Where(x => x.price >= minimumPrice.Value).ToList();
                }
                if (maximumPrice.HasValue)
                {
                    products = products.Where(x => x.price <= maximumPrice.Value).ToList();
                }
                if (sortBy.HasValue)
                {
                    switch (sortBy.Value)
                    {
                        case 2:
                            products = products.OrderByDescending(x => x.ID).ToList();
                            break;
                        case 3:
                            products = products.OrderBy(x => x.price).ToList();
                            break;
                        default:
                            products = products.OrderByDescending(x => x.price).ToList();
                            break;
                    }
                }
                return products.Count;
            }
        }

        public void DeleteProduct(int id)
        {
            using (var context = new SBContext())
            {
                var pid = context.Products.Find(id);
                context.Products.Remove(pid);
                context.SaveChanges(); //saved to database
            }
        }
        public void UpdateProduct(Product product)
        {
            using (var context = new SBContext())
            {
                 context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges(); //saved to database
            }
        }

        //function used to add to cart for checkout
        public List<Product> GetProducts(List<int> IDs)
        {
            using (var context = new SBContext())
            {
                return context.Products.Where(product => IDs.Contains(product.ID)).ToList();
            }
        }

        public int GetProductsCount(string search)
        {
            using (var context = new SBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Products.Count();
                }
            }
        }
        public Product GetProduct(int ID)
        {
            using (var context = new SBContext())
            {
                return context.Products.Where(x => x.ID == ID).Include(x => x.Category).FirstOrDefault();
            }
        }
        public List<Product> GetLatestProducts(int numberOfProducts)
        {
            using (var context = new SBContext())
            {
                return context.Products.OrderByDescending(x => x.ID).Take(numberOfProducts).Include(x => x.Category).ToList();
            }
        }

        public List<Product> GetProducts(int pageno)
        {
            int pageSize = 5;
            using (var context = new SBContext())
            {
                return context.Products.OrderBy(x => x.ID).Skip((pageno - 1) * pageSize).Take(pageSize).Include(x => x.Category).ToList();
            }
        }

        public List<Product> GetProducts(int pageno, int pageSize)
        {
            using (var context = new SBContext())
            {
                return context.Products.OrderByDescending(x => x.ID).Skip((pageno - 1) * pageSize).Take(pageSize).Include(x => x.Category).ToList();
            }
        }
        public List<Product> GetLatestProductsByCategory(int categoryID, int pageSize)
        {
            using (var context = new SBContext())
            {
                return context.Products.Where(x => x.Category.ID == categoryID).OrderByDescending(x => x.ID).Take(pageSize).Include(x => x.Category).ToList();
            }
        }
        public List<Product> GetProducts(string search, int pageno)
        {
            int pageSize = 5;
            //  pageSize = int.Parse(ConfigServices.Instance.GetConfig("ListingPageSize").value);
            using (var context = new SBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower()))
                                              .OrderBy(x => x.ID)
                                              .Skip((pageno - 1) * pageSize)
                                              .Take(pageSize)
                                              .Include(x => x.Category)
                                              .ToList();
                }
                else
                {
                    return context.Products.OrderBy(x => x.ID).Skip((pageno - 1) * pageSize).Take(pageSize).Include(x => x.Category).ToList();
                }
            }
        }


        public void SaveProducts(Product product)
        {
            using (var context = new SBContext())
            {
                context.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;

                context.Products.Add(product);//Instead of insert query - still in memory
                context.SaveChanges(); //saved to database
            }
        }
    }
}
