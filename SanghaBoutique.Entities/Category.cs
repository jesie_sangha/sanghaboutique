﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanghaBoutique.Entities
{
   public class Category : BaseEntity
    {
        //Category can have more then one product
        public List<Product> Product { get; set; }
        public string ImageURL { get; set; }
        public bool isFeatured { get; set; }
    }
}
