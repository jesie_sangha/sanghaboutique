﻿using SanghaBoutique.Database;
using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanghaBoutique.Services
{
   public class ConfigServices
    {
        public static ConfigServices Instance
        {
            get
            {
                if (instance == null)
                    instance = new ConfigServices();
                return instance;
            }
        }
        private static ConfigServices instance { get; set; } //create private object

        private ConfigServices()
        {
        }
        public Config GetConfig(string key)
        {
            using (var context = new SBContext())
            {
                return context.Configurations.Find(key);
            }
        }
    }
}
