﻿using SanghaBoutique.Database;
using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SanghaBoutique.Services
{

    public class CategoriesServices
    {
        // implemented Singleton design pattern
        public static CategoriesServices Instance
        {
            get
            {
                if (instance == null)
                    instance = new CategoriesServices();
                return instance;
            }
        }
        private static CategoriesServices instance { get; set; } //create private object
        private CategoriesServices()
        {
        }
        public void DeleteCategory(int id)
        {
            using (var context = new SBContext())
            {
                //  var cid = context.Categories.Find(id);

                var category = context.Categories.Where(x => x.ID == id).Include(x => x.Product).FirstOrDefault();
                context.Products.RemoveRange(category.Product);
                context.Categories.Remove(category);
                context.SaveChanges(); //saved to database
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new SBContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges(); //saved to database
            }
        }
        public Category GetCategory(int ID)
        {
            using (var context = new SBContext())
            {
                return context.Categories.Find(ID);
            }
        }

        public int GetCategoriesCount(string search)
        {
            using (var context = new SBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower())).Count();                                             
                }
                else
                {
                    return context.Categories.Count();                       
                }
            }
        }

        public List<Category> GetAllCategories()
        {
            using (var context = new SBContext())
            {              
                    return context.Categories.ToList();
            }
        }

        public List<Category> GetCategories(string search, int pageNo)
        {
            int pageSize = 3;
            //  pageSize = int.Parse(ConfigServices.Instance.GetConfig("ListingPageSize").value);
            using (var context = new SBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {

                    return context.Categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower()))
                                              .OrderBy(x => x.ID)
                                              .Skip((pageNo - 1) * pageSize)
                                              .Take(pageSize)
                                              .Include(x => x.Product)
                                              .ToList();
                }
                else
                {
                    return context.Categories
                        .OrderBy(x => x.ID)
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .Include(x => x.Product)
                        .ToList();
                }
            }
        }

        public List<Category> GetFeaturedCategories()
        {
            using (var context = new SBContext())
            {
               // var ctx = new dbEntities();
                var dep = context.Categories
                             .SqlQuery("SELECT * FROM Categories")
                             .FirstOrDefault();
                
                return context.Categories.Where(x => x.isFeatured && x.ImageURL != null).ToList();
           }
        }

        public void SaveCategories(Category category)
        {
            using (var context = new SBContext())
            {
                context.Categories.Add(category);//Instead of insert query - still in memory
                context.SaveChanges(); //saved to database
            }
        }
    }
}
