﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanghaBoutique.Entities
{
    public class Product : BaseEntity
    {
        //Every product belong to Category
        public virtual Category Category { get; set; }//virtual - bring data from category object

        [Required]      
        public decimal price { get; set; }

        public string ImageURL { get; set; }
        public int CategoryID { get; set; }
    }
}
