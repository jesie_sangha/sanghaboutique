﻿using SanghaBoutique.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanghaBoutique.Database
{
    //This class helps to access database which is created by Entity Framework
   public class SBContext : DbContext //Dbcontext is class of Entity Framework
    {
        //pass the name of connection string from App.config to constructor
        public SBContext():base("SanghaDatabaseConnection")
        {
        }
        //Validations - Fluent API
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().Property(p => p.Name).IsRequired().HasMaxLength(50);
        }
        //Generate below entities as tables in the database
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Config> Configurations { get; set; }
        //public DbSet<Order> Orders { get; set; }
        //public DbSet<OrderItem> OrderItems { get; set; }
    }
}
