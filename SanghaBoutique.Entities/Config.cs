﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SanghaBoutique.Entities
{
    //Store data related to social media or contact information
   public class Config
    {      
        [Key] //makes key property as primary key
        public string key { get; set; }
        public string value { get; set; }
    }
}
