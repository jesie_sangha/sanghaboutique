﻿using SanghaBoutique.Services;
using SanghaBoutique.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanghaBoutique.Web.Controllers
{
    public class WidgetController : Controller
    {
        // GET: Widget JAS
        public ActionResult Products(bool isLatestProduct, int? CategoryID=0)
        {
            ProductsWidgetViewModel model = new ProductsWidgetViewModel();
            model.isLatestProducts = isLatestProduct;
            if(isLatestProduct)
            {
                model.Products = ProductServices.Instance.GetLatestProducts(4);
            }
            else if(CategoryID.HasValue && CategoryID.Value>0)
            {
                model.Products = ProductServices.Instance.GetLatestProductsByCategory(CategoryID.Value, 4);
            }
            else
            {
                model.Products = ProductServices.Instance.GetProducts(1, 8);
            }
            return PartialView(model);
        }
    }
}